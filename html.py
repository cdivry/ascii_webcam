def html_frame(output):
    output_html = output.replace("\n", "<br>\n")
    output_html = output_html.replace(" ", "&nbsp;")
    html = """
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="refresh" content="1">
    <style>
      html, body {
        background-color: black;
        color: #fff;
        padding: 0;
        margin: 0;
        font-family: monospace;
        font-size: 7px;
      }
      body {
        text-align: center;
      }
      body:not(.loaded) {
        display:none;
      }
      ::-webkit-scrollbar {
        display: none;
      }
    </style>
  </head>
  <body onload="document.body.classList.add('loaded')">
    %(output_html)s
  </body>
</html>
    """ % {
        "output_html": output_html
    }
    with open("www/flux.html", "w") as fd:
        fd.write(html)


def txt_frame(output):
    output_txt = output.replace("\n", "<br>\n")
    output_txt = output_txt.replace(" ", "&nbsp;")
    with open("www/flux.txt", "w") as fd:
        fd.write(output_txt)
