#!/usr/bin/env python3

import argparse
import numpy as np
import cv2

import pygame
import pygame.camera

import time

from PIL import Image
from ascii_converter import convert_image_to_ascii
from html import html_frame, txt_frame


class Camera:

    def __init__(self):
        self.args = self.get_args()
        self.WIDTH = 640
        self.HEIGHT = 480
        self.FPS = 30
        self.ASCII_PADDING = "              "

    def run(self):
        if self.args.video != "":
            self.ascii_video()
        elif self.args.image != "":
            self.ascii_image()
        elif self.args.camera is True:
            self.ascii_camera()
        else:
            print("no --camera or --video option found")

    def get_args(self) -> dict:
        parser = argparse.ArgumentParser(
            prog='ASCII webcam',
            description='This display your camera input into terminal output (ASCII art)',
            epilog="cdivry"
        )
        parser.add_argument('-cam', '--camera', action='store_true', default=True)
        parser.add_argument('-vid', '--video', default="", type=str)
        parser.add_argument('-img', '--image', default="", type=str)
        parser.add_argument('-c', '--color', action='store_true', default=False)
        parser.add_argument('-dark', '--darkener', default=1, type=int)
        args = parser.parse_args()
        print("color:", args.color)
        print("darkener:", args.darkener)
        return args

    def ascii_camera(self):
        pygame.camera.init()
        camlist = pygame.camera.list_cameras()
        if camlist:
            cam = pygame.camera.Camera(camlist[0], (self.WIDTH, self.HEIGHT))
            cam.start()
            while True:
                webcam_image = cam.get_image()
                pil_image = pygame.image.tostring(
                    webcam_image,
                    "RGB",
                    False
                )
                image = Image.frombytes("RGB", (self.WIDTH, self.HEIGHT), pil_image)
                output = convert_image_to_ascii(
                    image,
                    self.args
                )
                padded_output = "\n".join([ self.ASCII_PADDING + line for line in output.split('\n')])
                print(padded_output)
        else:
            print("No camera on current device")

    def ascii_video(self):
        flux = cv2.VideoCapture(self.args.video)

        while True:
            more, frame_flux = flux.read()
            self.HEIGHT, self.WIDTH = frame_flux.shape[:2]

            if not all([more]):
                break

            image = Image.frombytes("RGB", (self.WIDTH, self.HEIGHT), frame_flux)
            output = convert_image_to_ascii(
                image,
                self.args
            )
            padded_output = "\n".join([ self.ASCII_PADDING + line for line in output.split('\n')])
            print(padded_output)

    def ascii_image(self):
        raw_image = Image.open(self.args.image, mode='r')
        output = convert_image_to_ascii(
            raw_image,
            self.args
        )
        padded_output = "\n".join([ self.ASCII_PADDING + line for line in output.split('\n')])
        print(padded_output)


if __name__ == "__main__":
    camera = Camera()
    camera.run()
