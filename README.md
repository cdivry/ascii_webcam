# ASCII Webcam

This software outputs in your terminal an ASCII version of your webcam.

![example of colored ASCII camera output](doc/image.png)

## Requirements :

```python
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Usage

### Static option (iframe):

Open `www/index_static.html` with your favorite browser, and run the camera script to update `www/flux.html`

```bash
./camera.py
```
### Javascript option:

This is the best framerate option.

First, run an HTTP server in a separated terminal window :
```bash
cd www/
./http_server.py
```

Then, open your favorite browser to http://localhost:7777/

Finally, run the camera script to update `www/flux.txt` :
```bash
./camera.py
```

You can test some options :
```bash
# enable color (in characters background)
./camera.py --color

# enable video stream conversion
./camera.py --video my_video.mp4

# simple image as input
./camera.py --image my_photo.png
```
