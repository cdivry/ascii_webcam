#!/usr/bin/env python3

import http.server
import socketserver


def http_server():
    PORT = 7777
    Handler = http.server.SimpleHTTPRequestHandler
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print("serving at port", PORT)
        httpd.serve_forever()


if __name__ == "__main__":
    http_server()
