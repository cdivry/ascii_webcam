import numpy as np
import sys

SCALE = 0.42
COLS = 150
gscale1 = "\$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~i!lI;:,^`'. "
gscale2 = "@%#*+=-:. "

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate

def get_average_greyscale(image):
    im = np.array(image)
    w, h = im.shape
    return np.average(im.reshape(w * h))

def get_average_rgb(image):
    avg_color_per_row = np.average(image, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    return avg_color

@static_vars(image=None, image_rgb=None, img_grey=None, color=None, avg_color=None, avg_grey=None)
def convert_image_to_ascii(image, args, more_levels=False):
    # average color of full image
    #convert_image_to_ascii.avg_color = get_average_rgb(
    #    convert_image_to_ascii.image_rgb
    #)
    global gscale1, gscale2
    convert_image_to_ascii.image = image.convert("L")
    convert_image_to_ascii.image_rbg = image.convert("RGB")
    W, H = convert_image_to_ascii.image.size[0], convert_image_to_ascii.image.size[1]
    w = W / COLS
    h = w / SCALE
    rows = int(H / h)
    if COLS > W or rows > H:
        print("Image too small for specified cols!")
        exit(0)
    convert_image_to_ascii.ascii_img = []
    for j in range(rows):
        y1 = int(j * h)
        y2 = int((j + 1) * h)

        if j == rows - 1:
            y2 = H
        tmp = []
        for i in range(COLS):
            x1 = int(i * w)
            x2 = int((i + 1) * w)
            if i == COLS - 1:
                x2 = W
            convert_image_to_ascii.img_grey = convert_image_to_ascii.image.crop((x1, y1, x2, y2))
            convert_image_to_ascii.avg_grey = int(get_average_greyscale(convert_image_to_ascii.img_grey))
            if more_levels:
                # gsval = gscale1[::-1][int((convert_image_to_ascii.avg_grey * len(gscale1)) / 255)]
                gsval = gscale1[::-1][int((convert_image_to_ascii.avg_grey * len(gscale1)) / 255)]
            else:
                gsval = gscale1[::-1][int((convert_image_to_ascii.avg_grey * len(gscale2)) / 255)]

            if args.color is True:
                convert_image_to_ascii.img_rgb = convert_image_to_ascii.image_rbg.crop((x1, y1, x2, y2))
                convert_image_to_ascii.color = get_average_rgb(
                    convert_image_to_ascii.img_rgb
                )
                rgb = (
                    int(convert_image_to_ascii.color[0] / args.darkener),
                    int(convert_image_to_ascii.color[1] / args.darkener),
                    int(convert_image_to_ascii.color[2] / args.darkener)
                )

                _rgb_bash = f"\x1b[48;2;{rgb[0]};{rgb[1]};{rgb[2]}m"
                tmp.append(f"{_rgb_bash}{gsval}")
            else:
                tmp.append(gsval)

        tmp.append("\x1b[0m")
        convert_image_to_ascii.ascii_img.append("".join(tmp))
    return "\n" + "\n".join(convert_image_to_ascii.ascii_img)
